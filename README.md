# Project Inbox #

Provides email handling for messsages sent to the [TPA Dashboard](https://bitbucket.org/compass_dataservices/project-dashboard).

Allows for handling emails addressed to a projects unit number without needing to create explicit email inboxes.