#!/usr/bin/env python3

from inbox import Inbox
from datetime import datetime
import os
import base64
import email
import re
import pickle

inbox_port = 25
inbox = Inbox(address='0.0.0.0', port=inbox_port)

UNIT_NUM_REGEX = re.compile(r'\d{4,5}')

@inbox.collate
def handle(to, sender, subject, body):
    #print('Received new email from {}\nTo:{}\nSubject:{}\nBody:{}'.format(sender, to, subject, body))
    for receipient in to:
        if receipient.split('@')[1] != 'compassfss.com' and receipient.split('@')[1] != 'fssdash.xyz':
            print('removing {} because it does not belong to this domain'.format(receipient))
            to.remove(receipient)
    if not to:
        print('no valid receipients found.  Aborting.')
        return
        
    data = email.message_from_string(body)
    file_name = re.sub('[^\w\-_\. ]', '_', subject)
    file_name = file_name.strip().replace(' ', '_')
    unit_number = get_unit_number(to, subject)
    if unit_number != '0':
        unit_path = os.path.join('/home/email', unit_number)
    else:
        destination = to[0].split('@')[0]
        unit_path = os.path.join('/home/email', destination)
        
    verify_directory_exists(unit_path)
    file_name = os.path.join(unit_path, file_name)
    parse_email(data, file_name)

    today = datetime.now().strftime('%y%m%d')
    file = '{}-{}-{}.p'.format(to[0], subject.replace(' ', '%32').replace(':', '%58'), today)
    file = os.path.join('/home/email/archive', file)
    try:
        with open(file, 'wb') as f:
            pickle.dump(body, f)
        now = datetime.now().strftime('%c')
        print('{} - email received and saved to {}'.format(now, file))
    except FileExistsError:
        print('{} already exists.'.format(file))
    except FileNotFoundError:
        print('Unable to save {}.  FileNotFoundError.'.format(file))


def parse_email(data, file_name):
    if data.is_multipart():
        msgs = data.get_payload()
        for msg in msgs:
            parse_email(msg, file_name)
    else:
        if data.get_content_type() == 'text/plain':
            save_email_as_txt(data, '{}.txt'.format(file_name))
        elif data.get_content_type() == 'text/html':
            save_email_as_html(data, '{}.html'.format(file_name))
        else:
            save_attachment(data, os.path.dirname(file_name))


def get_unit_number(receipients, subject):
    for recipient in receipients:
        match = re.search(UNIT_NUM_REGEX, recipient)
        break
    else:
        match = re.search(UNIT_NUM_REGEX, subject)

    if match:
        return match.group(0)
    else:
        return '0'


def verify_directory_exists(folder):
    try:
        os.mkdir(folder)
    except FileExistsError:
        pass


# Extract Attachment to file
def save_attachment(data, directory):
    with open(os.path.join(directory, data.get_filename()), 'wb') as f:
        f.write(data.get_payload(decode=True))


# Extract HTML cd email content
def save_email_as_html(data, file_name):
    with open(file_name, 'wb') as f:
        f.write(data.get_payload(decode=True))


def save_email_as_txt(data, file_name):
    if any('base64' in x for x in data.values()):
        # Convert base64 encoded email text to bytestring
        email_str = base64.b64decode(data.get_payload())

        # Convert bytestring to string
        email_text = email_str.decode('utf-8')

    else:
        email_text = data.get_payload()

    with open(file_name, 'w') as f:
        f.write(email_text.encode('ascii', 'ignore').decode('ascii'))


if __name__ == "__main__":
    now = datetime.now().strftime('%c')
    print('{} - inbox.py running on port {}...'.format(now, inbox_port))
    inbox.serve()
    print('inbox.py completed')
